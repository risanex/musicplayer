//
//  ViewController.swift
//  PlayerWithAVAudioPlayer
//
//  Created by MohammadAkbari on 17/08/2020.
//  Copyright © 2020 MohammadAkbari. All rights reserved.
//

import UIKit
import AVKit


class ViewController: UIViewController, AVAudioPlayerDelegate {

    var player : AVPlayer?
    var audioPlayer : AVAudioPlayer?
    var timer:Timer?
    
    @IBOutlet weak var btnPlay:UIButton!
    @IBOutlet weak var lblDuration:UILabel!
    @IBOutlet weak var slider:UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.preparePlayer(sound: "https://dls.music-fa.com/tagdl/99/Nooran%20-%20Bargard%20(128).mp3")
        
      
    }
    
    
    func preparePlayer(sound:String) {
        guard let url = URL(string:sound) else {
            print("Invalid URL")
            return
        }
        do {
            let session = AVAudioSession.sharedInstance()
            try session.setCategory(AVAudioSession.Category.playback)
            let soundData = try Data(contentsOf: url)
            audioPlayer = try AVAudioPlayer(data: soundData)
            audioPlayer?.prepareToPlay()
            audioPlayer?.volume = 0.7
            audioPlayer?.delegate = self
            self.slider.maximumValue = Float(audioPlayer?.duration ?? 0)
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(changeValueSliderWithCurrentTime), userInfo: nil, repeats: true)
            let minuteString = String(format: "%02d", (Int(audioPlayer!.duration) / 60))
            let secondString = String(format: "%02d", (Int(audioPlayer!.duration) % 60))
         //   self.lblDuration.text = "\(minuteString):\(secondString)"
            print("TOTAL TIMER: \(minuteString):\(secondString)")
        } catch {
            print(error)
        }
    }
    
    
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        audioPlayer?.stop()
        slider.value = 0
        btnPlay.setTitle("play", for: .normal)
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print(error.debugDescription)
    }

    @IBAction func play(_ sender: Any) {
        if audioPlayer?.isPlaying ?? true {
            audioPlayer?.pause()
            btnPlay.setTitle("play", for: .normal)
        } else {
            audioPlayer?.play()
            btnPlay.setTitle("Pause", for: .normal)
        }
    
    }
    
    @IBAction func changeValue(_ sender: Any) {
        
        
        audioPlayer?.pause()
        let curtime = slider.value
        audioPlayer?.currentTime = TimeInterval(curtime)
        audioPlayer?.play()
        if audioPlayer?.isPlaying ?? true {
            btnPlay.setTitle("Pause", for: .normal)
        } else {
            btnPlay.setTitle("Play", for: .normal)
        }
    }
    
    
    @objc func changeValueSliderWithCurrentTime() {
        let current = Float(audioPlayer?.currentTime ?? 0)
        let currentTime1 = Int(audioPlayer?.currentTime ?? 0)
        let minutes = currentTime1/60
        let seconds = currentTime1 - minutes * 60
        lblDuration.text = NSString(format: "%02d:%02d", minutes,seconds) as String
        slider.value = current
    }
}


